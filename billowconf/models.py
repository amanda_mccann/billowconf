###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class Conference(models.Model):
    name = models.CharField(max_length=255)
    irc_server = models.CharField(max_length=255, default='')

    def __str__(self):
        return self.name

class Room(models.Model):
    name = models.CharField(max_length=255)
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    stream = models.URLField()
    irc_channel = models.CharField(max_length=200, default='')

    def __str__(self):
        return self.name

class Talk(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    start = models.DateTimeField()
    duration = models.IntegerField()
    presenters = models.ManyToManyField(User)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    recorded_content = models.FileField(upload_to='talks/', blank=True, null=True)

    def __str__(self):
        return f"{self.name} ({self.presenters})"


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
