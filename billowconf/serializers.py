###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.contrib.auth.models import User, Group
from .models import Conference, Room, Talk
from rest_framework import serializers

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'id', 'name']

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'email', 'groups']

class ConferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conference
        fields = ['url', 'id', 'name', 'irc_server']

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['url', 'id', 'name', 'conference', 'stream', 'irc_channel']

class TalkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Talk
        fields = ['url', 'id', 'name', 'description', 'start', 'duration', 'presenters', 'room', 'recorded_content']
