# BillowConf

BillowConf is an online platform for virtual conferences. It supports different
rooms that people can join and interact with. Presenters give talks and can
enagage with the audience in real time through text (IRC) and video.

[Outline](https://writefreely.debian.social/paddatrapper/remote-conference-software)

[Planning](https://storm.debian.net/shared/D0kqX_gx1sYV01ipkucmkMnpKGoXEdLREEHLTRyWhRd)

## Install

```bash
$ virtualenv -p python3 pyenv
$ pyenv/bin/pip install -r requirements.txt
$ pyenv/bin/python manage.py createsuperuser
$ pyenv/bin/python manage.py migrate
$ pyenv/bin/python manage.py runserver
```
This will start a development web server on http://127.0.0.1:8000/

The frontend can then be run

```bash
$ cd frontend
$ npm install
$ npm run serve
```

## Debian Dependencies

Most of the node dependencies are already in Debian. It currently is missing
vuex and vuetify. The frontend modules need symlinking into
`./frontend/node_modules/` for `npm` to pick them up.

```bash
# Backend
$ sudo apt install python3-django python3-djangorestframework \
    python3-djangorestframework-filter python3-django-webpack-loader
# Frontend production dependencies
$ sudo apt install node-vue node-vue-hot-reload-api node-axios \
    node-core-js libjs-vue-router node-webrtc-adapter \
    fonts-materialdesignicons-webfont
# Frontend dev dependencies
$ sudo apt install node-babel-eslint node-bootstrap-sass node-vue-template-compiler npm
```
