const BundleTracker = require("webpack-bundle-tracker");

const pages = {
  "app": {
    entry: "./src/main.js",
    chunks: ["chunk-vendors"]
  }
}


module.exports = {
  pages: pages,
  transpileDependencies: ["vuetify"],
  filenameHashing: false,
  productionSourceMap: false,
  publicPath: process.env.NODE_ENV === "production"
    ? ""
    : "http://localhost:8080/",
  outputDir: "../billowconf/static/vue/",
  chainWebpack: config => {
    config.optimization
      .splitChunks({
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: "chunk-vendors",
            chunks: "all",
            priority: 1
          },
        },
      });
    config.resolve.alias.set("__STATIC__", "static")
    config
      .plugin("BundleTracker")
      .use(BundleTracker, [{filename: "webpack-stats.json"}]);
    config.devServer
      .public("http://localhost:8080")
      .host("localhost")
      .hotOnly(true)
      .watchOptions({poll: 1000})
      .https(false)
      .headers({"Access-Control-Allow-Origin": ["*"]})
  }
};
