import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';
import Api from '@/utils/api.js';


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    conference: {
      name: 'BillowConf',
      url: ''
    },
    rooms: [],
    token: localStorage.getItem('user-token') || '',
    status: ''
  },
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status
  },
  mutations: {
    conference(state, conference) {
      state.conference = conference;
      var conf = conference['id'];
      if (conf != '') {
        var api = new Api();
        api.rooms(conf).then(response => {
          state.rooms = response.data['results'];
        });
      }
    },
    'auth-request': (state) => {
      state.status = 'loading';
    },
    'auth-success': (state, token) => {
      state.status = 'success';
      state.token = token;
    },
    'auth-error': (state) => {
      state.status = 'error';
    },
    'auth-logout': (state) => {
      state.status = '';
      state.token = '';
    }
  },
  actions: {
    'auth-request': ({commit, dispatch}, user) => {
      return new Promise((resolve, reject) => {
        commit('auth-request');
        var api = new Api();
        api.login(user).then(response => {
          const token = response.data.token;
          localStorage.setItem('user-token', token);
          axios.defaults.headers.common['Authorization'] = token;
          commit('auth-success', token);
          dispatch('user-request')
          resolve(response);
        }).catch(error => {
          commit('auth-error', error);
          console.log(error);
          localStorage.removeItem('user-token');
          reject(error);
        });
      });
    },
    'auth-logout': ({commit}) => {
      return new Promise(resolve => {
        commit('auth-logout');
        localStorage.removeItem('user-token');
        delete axios.defaults.headers.common['Authorization'];
        resolve();
      });
    }
  },
  modules: {}
});
