import axios from 'axios';

/**
 * Manages the interaction with the backend API
 */
export default class Api {
  constructor() {}

  conference(id) {
    return axios.get(`/api/conferences/${id}/`);
  }

  rooms(conference) {
    return axios.get(`/api/rooms/?conference=${conference}`);
  }

  login(user) {
    return axios.post('/api/auth/token/', user);
  }
}
