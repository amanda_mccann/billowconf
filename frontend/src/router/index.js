import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import SignIn from '../views/SignIn.vue';
import AdminDashboard from '../views/AdminDashboard.vue';
import Logout from '../views/Logout.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/accounts/login/',
    name: 'SignIn',
    component: SignIn
  },
  {
    path: '/accounts/logout/',
    name: 'Logout',
    component: Logout
  },
  {
    path: '/admin/',
    name: 'AdminDashboard',
    component: AdminDashboard
  },

];

const router = new VueRouter({
  mode: 'history',
  base: '',
  routes
});

export default router;
